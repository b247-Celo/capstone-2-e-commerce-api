const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");

const auth = require("../auth");

//Route for creating a product with admin only
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all", (req,res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving all active products
router.get("/active", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// S44 Route for retrieving specific product by id
router.get("/:productId/details", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating product by id Admin only
router.put("/:id/update", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for archiving product by id Admin only
router.put("/:id/archive", auth.verify, (req,res) => {

	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for activating a product
router.put('/:id/activate', auth.verify, (req, res) => {
	
	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;
