const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");



// Route for user creating an order
router.post('/checkout', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).userId,
		reqBody: req.body
	}

	 orderController.checkoutOrder(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;

