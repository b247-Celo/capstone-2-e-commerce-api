const express = require("express");
const router = express.Router();

const auth = require("../auth");
const userController = require("../controllers/userController");

//Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for getting user details
router.get("/:id/userDetails", auth.verify, (req, res) => {

       const userData = auth.decode(req.headers.authorization);

	userController.detailUser({userId : userData.id}).then(result => res.send(result));
})



module.exports = router;