const Product = require("../models/Product");

// Controller for adding a product admin only
module.exports.addProduct = (data) => {

	if(data.isAdmin){

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return {
					message: "New product successfully created!"
				}
			};
		});

	} 

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	return message.then((value) => {
		return value;
	})

};

// Controller for retrieving all active products
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result
	})
}


// Controller for retrieving all active products
module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};


// Controller for retrieving specific product by id
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};

// Controller for updating product by id admin only
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.id, updatedProduct).then((course, error) => {

		if(error){
			return false
		} else {
			let message = `Successfully updated product Id - "${reqParams.id}"`

			return message;
		}
	})
}

// Controller for archiving a product by id admin only
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.id, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// Controller for activating a product -admin only
module.exports.activateProduct = (reqParams) => {
	let activatedProduct = {
		isActive : true
	}
	return Product.findByIdAndUpdate(reqParams.id, activatedProduct).then((product, err) => {
		let message = "";
		if (err) {
			message = "Product activation failed! Please select another product."
			return message;
		} else {
			message = "Product activated!"
			return message;
		};
	});
};



	
	