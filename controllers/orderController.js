const Order = require("../models/Order");
//const User = require("../models/User");
//const bcrypt = require("bcrypt");
//const auth = require("../auth");
const Product = require("../models/Product");


//Controller for creating an order


 

module.exports.checkoutOrder = async(data)=>{

       if(data.isAdmin !== true){

        const newOrder = new Order({

            userId : data.userId,
            products: data.reqBody.products,
            totalAmount: data.reqBody.totalAmount,

        });

        const updateOrder =  await newOrder.save().then((order,error)=>{

                if(error){
                    return false;
                }
                else{
                    return order;
                }

             });

     
        let updateProduct;

        updateOrder.products.forEach(product=>{
           
            updateProduct = Product.findById(product.productId).then(orderProduct=>{

      
                    orderProduct.orders.push({orderId: product.id,quantity: product.quantity});

                    return orderProduct.save().then((order,error)=>{

                                if(error){
                                    return false;
                                }
                                else{
                                    return true;
                                }
                            
                            })         
                       })
                 })

    return updateProduct?{messaage: "Order placed succesfully."}:false;

    }
    else{
        return {Message: "Login to your non-Admin account."}
    }


}