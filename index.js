const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors")

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const app = express();

const port = 5000;



mongoose.connect("mongodb+srv://rodjCap2:rodj123@cluster0.hpvwekf.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// CORS stands for Cross-Origin Resources Sharing. 
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Declaring main routes
app.use("/users", userRoutes);

app.use("/products", productRoutes);
app.use("/orders", orderRoutes);







app.listen(port, () => console.log(`Server is running at localhost: ${port}`));